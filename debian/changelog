powershell-empire (5.9.5-1parrot1) stable; urgency=medium

  * New upstream version 5.9.5

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Wed, 28 Feb 2024 16:13:34 +0700

powershell-empire (5.9.4-1parrot1) stable; urgency=medium

  * Add .venv to install
  * Remove .so files
  * Remove all .so file with version number in name
  * Add an other command to remove .so files
  * Fix wrong wildcard in finding .so files
  * New upstream version 5.9.4

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Wed, 21 Feb 2024 16:14:05 +0700

powershell-empire (5.8.4-1parrot1) stable; urgency=medium

  * Include new CI
  * Fix libpython version
  * Try new build with Debian's python3-poetry
  * Remove venv
  * Restore debian fixperms
  * Add runtime dependencies
  * Remove venv in start script
  * Replace python3-all by ${python3:Depends}
  * Replace python3:depends by python3
  * Add python3-poetry to depends
  * Use the poetry to run empire
  * Add toml file to installation location
  * Add cd to run powershell empire
  * Restore poetry build rule
  * Restore dh_install line
  * Restore override (commented by mistake)
  * Remove runtime dependencies
  * New upstream version 5.8.4

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Mon, 25 Dec 2023 12:24:26 +0700

powershell-empire (5.7.3-1parrot1) parrot-updates; urgency=medium

  * New upstream version 5.7.3

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Thu, 16 Nov 2023 23:00:25 +0700

powershell-empire (5.6.3-0parrot2) parrot-updates; urgency=medium

  * Add CI build

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Fri, 27 Oct 2023 06:55:20 +0700

powershell-empire (5.6.3-0parrot1) parrot-updates; urgency=medium

  * New upstream version 5.6.3

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Sat, 02 Sep 2023 19:12:55 +0700

powershell-empire (5.0.4-0parrot5) parrot-updates; urgency=medium

  * No longer move configs

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Fri, 03 Mar 2023 16:41:08 +0700

powershell-empire (5.0.4-0parrot4) parrot-updates; urgency=medium

  * Force start mariadb server
  * Remove set -e
  * Force create empty dir "profiles"
  * remove old dirs
  * Remove windows resources link

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Fri, 03 Mar 2023 16:28:14 +0700

powershell-empire (5.0.4-0parrot3) parrot-updates; urgency=medium

  * Fix empty line in section

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Fri, 03 Mar 2023 16:12:03 +0700

powershell-empire (5.0.4-0parrot2) parrot-updates; urgency=medium

  * Remove file VERSION
  * Update new path of doc
  * Remove invalid packages
  * Refresh runtime dependencies
  * Remove dh_python, which cause error when do postinst
  * Add commands to generate database
  * Fix syntax of postinst
  * Use venv build to solve incompatible packages

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Fri, 03 Mar 2023 16:11:11 +0700

powershell-empire (5.0.4-0parrot1) parrot-updates; urgency=medium

  * New upstream version 5.0.4

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Tue, 28 Feb 2023 09:30:50 +0700

powershell-empire (5.0.3-0parrot1) parrot-updates; urgency=medium

  * New upstream version 5.0.3

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Thu, 23 Feb 2023 07:00:47 +0700

powershell-empire (4.10.0-0parrot1) parrot-updates; urgency=medium

  * New upstream version 4.10.0

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Fri, 03 Feb 2023 23:32:11 +0700

powershell-empire (4.9.0-0parrot2) parrot-updates; urgency=medium

  * Removee debian patches

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Sun, 25 Dec 2022 07:34:00 +0700

powershell-empire (4.9.0-0parrot1) parrot-updates; urgency=medium

  * New upstream version 4.9.0

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Sun, 25 Dec 2022 07:30:31 +0700

powershell-empire (4.1.3-0parrot2) parrot-updates; urgency=medium

  * Add watch file for automation update

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Sun, 25 Dec 2022 07:27:33 +0700

powershell-empire (4.1.3-0parrot1) lts-updates; urgency=medium

  * Rebuild for Parrot 5.0.

 -- Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>  Wed, 13 Oct 2021 18:05:30 +0200

powershell-empire (4.1.3-0kali2) kali-dev; urgency=medium

  * Improve postinst to automatically reset the DB if the previous version is
    lower than 4.1.0~ (see bug 5967)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 12 Oct 2021 14:51:11 +0200

powershell-empire (4.1.3-0kali1) kali-dev; urgency=medium

  * New upstream version 4.1.3
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 28 Sep 2021 13:36:25 +0200

powershell-empire (4.1.2-0kali1) kali-dev; urgency=medium

  * New upstream version 4.1.2
  * Update debian/copyright
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 21 Sep 2021 09:06:59 +0200

powershell-empire (4.1.0-0kali1) kali-dev; urgency=medium

  * New upstream version 4.1.0
  * Refresh patches
  * Add missing depends python3-jq

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 31 Aug 2021 15:31:17 +0200

powershell-empire (4.0.2-0kali1) kali-dev; urgency=medium

  * New upstream version 4.0.2
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 17 Aug 2021 18:17:17 +0200

powershell-empire (4.0.1-0kali1) kali-dev; urgency=medium

  * New upstream version 4.0.1
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 09 Aug 2021 15:45:12 +0200

powershell-empire (4.0.0+gitsubmodule-0kali2) kali-dev; urgency=medium

  * Fix broken link
  * Fix the postrm

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 12 Jul 2021 15:25:45 +0200

powershell-empire (4.0.0+gitsubmodule-0kali1) kali-experimental; urgency=medium

  * Re-import upstream version 4.0.0 with missing submodules

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 29 Jun 2021 10:24:35 +0200

powershell-empire (4.0.0-0kali1) kali-experimental; urgency=medium

  * New upstream version 4.0.0
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 28 Jun 2021 11:00:35 +0200

powershell-empire (4.0.0~beta.2-0kali1) kali-experimental; urgency=medium

  * New upstream version 4.0.0~beta.2
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 03 Jun 2021 08:55:32 +0200

powershell-empire (4.0.0~alpha.3-0kali1) kali-experimental; urgency=medium

  * New upstream version 4.0.0~alpha.3
  * Remove old flask-socketio from package: we can now use the
    python3-flask-socketio from Debian
  * Refresh patches and remove debian/patches/force-async-mode.patch merged
    usptream

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 18 May 2021 11:12:39 +0200

powershell-empire (4.0.0~alpha.2-0kali2) kali-experimental; urgency=medium

  * Do not remove gitignore: it deletes the directories too

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 07 May 2021 16:19:16 +0200

powershell-empire (4.0.0~alpha.2-0kali1) kali-experimental; urgency=medium

  * New upstream version 4.0.0~alpha.2

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 05 May 2021 08:39:48 +0200

powershell-empire (4.0.0~alpha.1-0kali1) kali-experimental; urgency=medium

  * Remove obsolete debian/watch
  * New upstream version 4.0.0~alpha.1
  * Refresh patches
  * Remove python3-m2crypto dependency
  * Update installation
  * Install config files *.yaml in /etc
  * Update debian/copyright
  * Refresh lintian-overrides
  * Add missing depends
  * Fix permissions
  * Bump Standards-Version to 4.5.1
  * Update flask-socketio
  * Update socketio
  * Remove useless dependency on python3-gevent-websocket
  * Add python3-websocket and python3-websocket-client as dependencies
  * Add a patch to force async_mode to 'threading'
  * Add dotnet-sdk-3.1 in Recommends
  * Reset database if upgrade from version lower than 4.0.0~

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 30 Apr 2021 15:51:53 +0200

powershell-empire (3.8.2-0kali1) kali-dev; urgency=medium

  * New upstream version 3.8.2

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 29 Mar 2021 11:02:24 +0200

powershell-empire (3.8.1-0kali1) kali-dev; urgency=medium

  * Add a patch to fix syntax warning
  * New upstream version 3.8.1

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 23 Mar 2021 10:17:13 +0100

powershell-empire (3.8.0-0kali1) kali-dev; urgency=medium

  * New upstream version 3.8.0
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 08 Mar 2021 16:53:57 +0100

powershell-empire (3.7.2-0kali2) kali-dev; urgency=medium

  * Add missing depends: python3-gevent-websocket

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 15 Feb 2021 16:31:30 +0100

powershell-empire (3.7.2-0kali1) kali-dev; urgency=medium

  * New upstream version 3.7.2
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 08 Feb 2021 11:44:38 +0100

powershell-empire (3.7.1-0kali1) kali-dev; urgency=medium

  * Add debian/README.source
  * New upstream version 3.7.1

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 28 Jan 2021 09:54:46 +0100

powershell-empire (3.7.0-0kali3) kali-dev; urgency=medium

  * Add compatible socketio and engineio python modules with flask-socketio
    version 4.3.1

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 20 Jan 2021 10:39:08 +0100

powershell-empire (3.7.0-0kali2) kali-dev; urgency=medium

  * Add embedded flask-socketio 4.3.1: we have version 5.* in Kali but it's
    not yet supported

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 20 Jan 2021 09:31:40 +0100

powershell-empire (3.7.0-0kali1) kali-dev; urgency=medium

  * Add missing dependencies
  * Install missing config.yaml
  * Refresh patches
  * New upstream version 3.7.0
  * Update debian/copyright for cli/* files

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 19 Jan 2021 09:13:47 +0100

powershell-empire (3.6.3-0kali1) kali-dev; urgency=medium

  * New upstream version 3.6.3

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 22 Dec 2020 09:20:05 +0100

powershell-empire (3.6.2-0kali1) kali-dev; urgency=medium

  * New upstream version 3.6.2
  * Refresh patches
  * Update patches to use sudo

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 30 Nov 2020 15:35:03 +0100

powershell-empire (3.6.0-0kali1) kali-dev; urgency=medium

  * New upstream version 3.6.0

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 09 Nov 2020 15:54:58 +0100

powershell-empire (3.5.2-0kali1) kali-dev; urgency=medium

  * New upstream version 3.5.2
  * Refresh patches
  * Use cyrptodome instead of crypto

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 27 Oct 2020 10:26:07 +0100

powershell-empire (3.5.1-0kali1) kali-dev; urgency=medium

  * New upstream version 3.5.1
  * Add missing dependencies
  * Refresh debian/patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 19 Oct 2020 14:13:09 +0200

powershell-empire (3.4.0-0kali1) kali-dev; urgency=medium

  * New upstream version 3.4.0

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 16 Sep 2020 15:09:33 +0200

powershell-empire (3.3.4-0kali1) kali-dev; urgency=medium

  * New upstream version 3.3.4

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 26 Aug 2020 17:46:00 +0200

powershell-empire (3.3.2-0kali1) kali-dev; urgency=medium

  * New upstream version 3.3.2

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 06 Aug 2020 11:22:31 +0200

powershell-empire (3.3.1-0kali2) kali-dev; urgency=medium

  * Fix issue with database location (see 6623)

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 06 Aug 2020 10:52:19 +0200

powershell-empire (3.3.1-0kali1) kali-dev; urgency=medium

  * New upstream version 3.3.1
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 21 Jul 2020 11:25:07 +0200

powershell-empire (3.2.3-0kali1) kali-dev; urgency=medium

  * New upstream version 3.2.3
  * Refresh patches
  * Add a News about the database changes

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 09 Jun 2020 14:29:30 +0200

powershell-empire (3.2.2-0kali1) kali-dev; urgency=medium

  * New upstream version 3.2.2

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 27 May 2020 14:46:43 +0200

powershell-empire (3.2.1-0kali1) kali-dev; urgency=medium

  * New upstream version 3.2.1

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 11 May 2020 12:00:53 +0200

powershell-empire (3.2.0-0kali1) kali-dev; urgency=medium

  * New upstream version 3.2.0

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 29 Apr 2020 14:46:05 +0200

powershell-empire (3.1.4-0kali2) kali-dev; urgency=medium

  * Refresh patches:
     - fix reset-empire script
     (https://gitlab.com/kalilinux/packages/powershell-empire/-/issues/3)
     - change path for empire.debug and downloads

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 15 Apr 2020 12:00:26 +0200

powershell-empire (3.1.4-0kali1) kali-dev; urgency=medium

  * New upstream version 3.1.4

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 06 Apr 2020 09:29:13 +0200

powershell-empire (3.1.3-0kali1) kali-dev; urgency=medium

  * New upstream version 3.1.3

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 23 Mar 2020 10:43:11 +0100

powershell-empire (3.1.2-0kali1) kali-dev; urgency=medium

  * New upstream version 3.1.2

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 17 Mar 2020 09:43:18 +0100

powershell-empire (3.1.1-0kali1) kali-dev; urgency=medium

  * New upstream version 3.1.1

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 12 Mar 2020 09:31:04 +0100

powershell-empire (3.1.0-0kali1) kali-dev; urgency=medium

  * New upstream version 3.1.0

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 05 Mar 2020 16:33:18 +0100

powershell-empire (3.0.7-0kali1) kali-dev; urgency=medium

  * New upstream version 3.0.7

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 10 Feb 2020 14:46:48 +0100

powershell-empire (3.0.6-0kali1) kali-dev; urgency=medium

  * Update debian/README.Debian
  * New upstream version 3.0.6
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 31 Jan 2020 16:02:13 +0100

powershell-empire (3.0.5-0kali1) kali-dev; urgency=medium

  * New upstream version 3.0.5
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 21 Jan 2020 14:56:57 +0100

powershell-empire (3.0.3-0kali1) kali-dev; urgency=medium

  * New upstream version 3.0.3
  * Refresh patches
  * Remove useless debian/source/include-binaries
  * Fix syntax errors

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 14 Jan 2020 09:55:24 +0100

powershell-empire (3.0.0-0kali1) kali-dev; urgency=medium

  * New upstream version 3.0.0
  * Add missing depends python3-simplejson
  * Bump Standards-Version to 4.4.1
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 27 Dec 2019 09:26:26 +0100

powershell-empire (3.0~git20191203-0kali3) kali-experimental; urgency=medium

  * Put database in ~/.local/powershell-empire

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 06 Dec 2019 09:01:22 +0100

powershell-empire (3.0~git20191203-0kali2) kali-experimental; urgency=medium

  * Refresh patches
  * Change database location: in user dir, not in powershell-empire dir
  * Add a patch to use Python 3 everywhere

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 05 Dec 2019 16:03:03 +0100

powershell-empire (3.0~git20191203-0kali1) kali-experimental; urgency=medium

  * Import dev tarball for tests
  * Switch to Python 3
  * Change Upstream homepage
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 04 Dec 2019 15:06:06 +0100

powershell-empire (2.5-0kali3) kali-dev; urgency=medium

  * Switch install location (back) and symlink

 -- Ben Wilson <g0tmi1k@kali.org>  Mon, 17 Jun 2019 13:38:48 +0100

powershell-empire (2.5-0kali2) kali-dev; urgency=medium

  * Switch install location

 -- Ben Wilson <g0tmi1k@kali.org>  Mon, 17 Jun 2019 13:06:47 +0100

powershell-empire (2.5-0kali1) kali-dev; urgency=medium

  * Initial release (Closes: 4599)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 23 Apr 2019 11:49:53 +0200
